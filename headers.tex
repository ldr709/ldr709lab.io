% Options for packages loaded elsewhere
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}

\documentclass[11pt]{article}

\usepackage[maxcitenames=99]{biblatex}
\addbibresource{abbrev3.bib}
\addbibresource{pubs.bib}

\usepackage{amsmath,amsfonts}
\usepackage[unicode]{hyperref}

\usepackage{ifxetex,ifluatex}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{textcomp} % provide euro and other symbols
\usepackage{upquote} % for straight quotes in verbatim environments
\usepackage[protrusion=true,expansion=true]{microtype}

\usepackage{xcolor}
\usepackage{xurl} % add URL line breaks

\usepackage{bookmark}

\newcommand{\tmpmarginparwidth}{1.1in}

\newcommand{\vmarginsize}{2cm}
\newcommand{\hmarginsize}{1.4in}
\usepackage[paperwidth=8.5in,paperheight=11in,top=\vmarginsize,bottom=\vmarginsize,left=\hmarginsize,right=\hmarginsize,marginparwidth=\tmpmarginparwidth]{geometry}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz,multirow,booktabs,adjustbox}
\usepackage{algpseudocode, algorithmicx}
\usepackage{etoolbox}
\usepackage{enumitem}
\usepackage{parskip}
\usepackage{seqsplit}
\usepackage{graphicx}
\usepackage{xspace,xcolor}
\newcommand{\pct}{\mathbin{\%}}
% makes ":=" aligned better
\usepackage{mathtools}
\usepackage{newunicodechar}
\usepackage{titlesec}
\usepackage[super]{nth}
\usepackage{siunitx}
\usepackage{fontawesome5}
\mathtoolsset{centercolon}

\ifdefined\HCode
\usepackage{titling}
\fi

\hypersetup{
  pdflinkmargin=1pt,
  pdftitle={Curriculum Vitæ},
  colorlinks=true,
  linkcolor=Maroon,
  filecolor=Maroon,
  citecolor=Blue,
  urlcolor=Blue,
}
\urlstyle{same} % disable monospaced font for URLs

\ifdefined\HCode
% tex4ht barfs on using the stared version of a DeclarePairedDelimiter, so disable it.
\makeatletter
\MHInternalSyntaxOn
\let\OldDeclarePairedDelimiter\DeclarePairedDelimiter
\renewcommand{\DeclarePairedDelimiter}[3]{%
\OldDeclarePairedDelimiter{#1}{#2}{#3}%
\renewcommand{#1}{\@ifstar%
{\csname MT_delim_\MH_cs_to_str:N #1 _nostar:\endcsname}%
{\csname MT_delim_\MH_cs_to_str:N #1 _nostar:\endcsname}}%
}
\MHInternalSyntaxOff
\makeatother

% tex4ht also has a bug when handling \texttt. The extra layer of \text seems to fix it.
\let\Oldtexttt\texttt
\renewcommand{\texttt}[1]{\text{\Oldtexttt{#1}}}
\fi

\ifdefined\HCode
\newcommand\textlf[1]{{%
	\HCode{<span class="textlf">}%
	{#1}%
	\HCode{</span>}%
}}
\else
\newcommand\textlf[1]{{\fontfamily{CrimsonPro-OsF}\fontseries{el}\selectfont #1}}
\fi

\ifdefined\HCode
\renewcommand\textrm[1]{{%
	\HCode{<span class="textrm">}%
	{#1}%
	\HCode{</span>}%
}}
\renewcommand\textsf[1]{{%
	\HCode{<span class="textsf">}%
	{#1}%
	\HCode{</span>}%
}}
\renewcommand\texttt[1]{{%
	\HCode{<span class="texttt">}%
	{#1}%
	\HCode{</span>}%
}}
\fi

\ifdefined\HCode
\makeatletter
\renewcommand\hspace[1]{%
	\leavevmode%
	\HCode{<span style="margin-right: #1;">&\#8203;;</span>}%
}
\renewcommand\quad{\hspace{1em}}
\renewcommand\qquad{\hspace{2em}}
\newcommand\reallyvspace[1]{%
	\HCode{<span style="padding-bottom: #1; display: block"></span>}%
	\vspace{0pt}%
	\leavevmode
}
\makeatother
\else
\newcommand\reallyvspace[1]{\vspace{#1}}
\fi

\author{Lawrence Roy}

\newcommand{\false}{\ensuremath{\textsc{false}}\xspace}
\newcommand{\true}{\ensuremath{\textsc{true}}\xspace}

\newcommand{\myvec}[1]{\boldsymbol{#1}}
\newcommand{\algorithm}[1]{\ensuremath{\text{\sf #1}}\xspace}
\newcommand{\defeq}{\overset{\textrm{def}}=}

%%%%%%%%%%%%%%%%%%%%

\newcommand{\etal}{{\sl et~al.~}}
\newcommand{\eg}{{\sl e.g.}}
\newcommand{\ie}{{\sl i.e.}}
\newcommand{\apriori}{{\sl a~priori\/}\xspace}

% indistinguishability operator
% http://tex.stackexchange.com/questions/22168/triple-approx-and-triple-approx-with-a-straight-middle-line
\newcommand{\indist}{  \mathrel{\vcenter{\offinterlineskip
  \hbox{$\sim$}\vskip-.35ex\hbox{$\sim$}\vskip-.35ex\hbox{$\sim$}}}}
\renewcommand{\cong}{\indist}

% Double angle brackets
\ifdefined\HCode
% Idea from https://tex.stackexchange.com/a/223833/73340
\newcommand{\llangle}{{\special{t4ht@+\string&{35}x27EA{59}}x}}
\newcommand{\rrangle}{{\special{t4ht@+\string&{35}x27EB{59}}x}}
\else
% Relevant portion of MnSymbol package. From https://tex.stackexchange.com/a/79701/73340
\makeatletter
\DeclareFontFamily{OMX}{MnSymbolE}{}
\DeclareSymbolFont{MnLargeSymbols}{OMX}{MnSymbolE}{m}{n}
\SetSymbolFont{MnLargeSymbols}{bold}{OMX}{MnSymbolE}{b}{n}
\DeclareFontShape{OMX}{MnSymbolE}{m}{n}{
    <-6>  MnSymbolE5
   <6-7>  MnSymbolE6
   <7-8>  MnSymbolE7
   <8-9>  MnSymbolE8
   <9-10> MnSymbolE9
  <10-12> MnSymbolE10
  <12->   MnSymbolE12
}{}
\DeclareFontShape{OMX}{MnSymbolE}{b}{n}{
    <-6>  MnSymbolE-Bold5
   <6-7>  MnSymbolE-Bold6
   <7-8>  MnSymbolE-Bold7
   <8-9>  MnSymbolE-Bold8
   <9-10> MnSymbolE-Bold9
  <10-12> MnSymbolE-Bold10
  <12->   MnSymbolE-Bold12
}{}
\let\llangle\@undefined
\let\rrangle\@undefined
\DeclareMathDelimiter{\llangle}{\mathopen}%
                     {MnLargeSymbols}{'164}{MnLargeSymbols}{'164}
\DeclareMathDelimiter{\rrangle}{\mathclose}%
                     {MnLargeSymbols}{'171}{MnLargeSymbols}{'171}
\makeatother
\fi

\DeclarePairedDelimiter\abs{\lvert}{\rvert}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\DeclarePairedDelimiter\round{\lfloor}{\rceil}
\DeclarePairedDelimiter\lrangle{\langle}{\rangle}
\DeclarePairedDelimiter\LRangle{\llangle}{\rrangle}

\newcommand{\len}[1]{\abs{#1}}

\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}

\newcommand{\es}{\emptyset}
\newcommand{\bra}[1]{\left\{#1\right\}}
\newcommand{\alt}{\ |\ }
\newcommand{\class}[1]{\mathsf{#1}}
\renewcommand{\P}{\class{P}}
\newcommand{\NP}{\class{NP}}
\newcommand{\coNP}{\class{coNP}}
\newcommand{\PP}{\class{PP}}
\newcommand{\BPP}{\class{BPP}}
\newcommand{\EXP}{\class{EXP}}
\newcommand{\NL}{\class{NL}}
\newcommand{\comp}[1]{\overline{#1}}
\newcommand{\xor}{\oplus}
\newcommand{\emod}[1]{\equiv_{#1}}
\newcommand{\cat}{\parallel}
\newcommand{\compeq}{\stackrel{?}{=}}

\newcommand{\journalname}[1]{#1}

\DeclareMathOperator{\lcm}{lcm}
\DeclareMathOperator\trace{tr}
\DeclareMathOperator\adj{adj}
\DeclareMathOperator\lsb{lsb}

\DeclareMathOperator{\Proof}{Proof}

% fancy script L
\usepackage[mathscr]{euscript}
\renewcommand{\L}{\ensuremath{\mathscr{L}}\xspace}
\newcommand{\lib}[2][]{{\ensuremath{\L^{#1}_{\textsf{#2}}}\xspace}}
\newcommand{\adv}[1]{\ensuremath{\mathscr{A}_{\textsf{#1}}}\xspace}


\newcommand{\myterm}[1]{\ensuremath{\text{#1}}\xspace}
\newcommand{\bias}{\myterm{bias}}
\newcommand{\link}{\diamond}
\newcommand{\subname}[1]{\ensuremath{\textsc{#1}}\xspace}

%% colors
\definecolor{highlightcolor}{HTML}{F5F5A4}
\definecolor{highlightcolor2}{HTML}{001030}
\definecolor{highlighttextcolor}{HTML}{000000}
\definecolor{highlightawardtext}{HTML}{00770b}
\definecolor{bitcolor}{HTML}{a91616}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newunicodechar{α}{\ensuremath{\alpha}}
\newunicodechar{Α}{\ensuremath{\Alpha}}
\newunicodechar{β}{\ensuremath{\beta}}
\newunicodechar{Β}{\ensuremath{\Beta}}
\newunicodechar{γ}{\ensuremath{\Gamma}}
\newunicodechar{Γ}{\ensuremath{\Gamma}}
\newunicodechar{δ}{\ensuremath{\delta}}
\newunicodechar{Δ}{\ensuremath{\Delta}}
\newunicodechar{ε}{\ensuremath{\epsilon}}
\newunicodechar{Ε}{\ensuremath{\Epsilon}}
\newunicodechar{ζ}{\ensuremath{\zeta}}
\newunicodechar{Ζ}{\ensuremath{\Zeta}}
\newunicodechar{θ}{\ensuremath{\theta}}
\newunicodechar{Θ}{\ensuremath{\Theta}}
\newunicodechar{λ}{\ensuremath{\lambda}}
\newunicodechar{Λ}{\ensuremath{\Lambda}}
\newunicodechar{ξ}{\ensuremath{\xi}}
\newunicodechar{Ξ}{\ensuremath{\Xi}}
\newunicodechar{π}{\ensuremath{\pi}}
\newunicodechar{Π}{\ensuremath{\Pi}}
\newunicodechar{ρ}{\ensuremath{\rho}}
\newunicodechar{Ρ}{\ensuremath{\Rho}}
\newunicodechar{σ}{\ensuremath{\sigma}}
\newunicodechar{Σ}{\ensuremath{\Sigma}}
\newunicodechar{τ}{\ensuremath{\tau}}
\newunicodechar{Τ}{\ensuremath{\Tau}}
\newunicodechar{φ}{\ensuremath{\varphi}}
\newunicodechar{Φ}{\ensuremath{\Phi}}
\newunicodechar{χ}{\ensuremath{\chi}}
\newunicodechar{Χ}{\ensuremath{\Chi}}
\newunicodechar{ψ}{\ensuremath{\psi}}
\newunicodechar{Ψ}{\ensuremath{\Psi}}
\newunicodechar{ω}{\ensuremath{\omega}}
\newunicodechar{Ω}{\ensuremath{\Omega}}

\newunicodechar{†}{\ensuremath{\dagger}}

%%%%%%%%%%%%%%%%%%%%

%%% boxes for writing libraries/constructions
\usepackage{varwidth}

\newcommand{\codebox}[1]{%
        \begin{varwidth}{\linewidth}%
        \begin{tabbing}%
            ~~~\=\quad\=\quad\=\quad\=\kill % initialize tabstops
            #1
        \end{tabbing}%
        \end{varwidth}%
}
\newcommand{\titlecodebox}[2]{%
    \fboxsep=0pt%
    \fcolorbox{black}{black!10}{%
        \begin{varwidth}{\linewidth}%
        \centering%
        \fboxsep=3pt%
        \colorbox{black!10}{#1} \\
        \colorbox{white}{\codebox{#2}}%
        \end{varwidth}%
    }
}
\newcommand{\fcodebox}[1]{%
    \framebox{\codebox{#1}}%
}
\newcommand{\hlcodebox}[1]{%
    \fcolorbox{black}{highlightcolor}{\codebox{#1}}%
}
\newcommand{\hltitlecodebox}[2]{%
    \fboxsep=0pt%
    \fcolorbox{black}{black!15!highlightcolor}{%
        \begin{varwidth}{\linewidth}%
        \centering%
        \fboxsep=3pt%
        \colorbox{black!15!highlightcolor}{\color{highlighttextcolor}#1} \\
        \colorbox{highlightcolor}{\color{highlighttextcolor}\codebox{#2}}%
        \end{varwidth}%
    }
}


%% highlighting
\newcommand{\basehighlight}[1]{\colorbox{highlightcolor}{\color{highlighttextcolor}#1}}
\newcommand{\mathhighlight}[1]{\basehighlight{$#1$}}
\newcommand{\highlight}[1]{\raisebox{0pt}[-\fboxsep][-\fboxsep]{\basehighlight{#1}}}
\newcommand{\highlightline}[1]{%\raisebox{0pt}[-\fboxsep][-\fboxsep]{
    \hspace*{-\fboxsep}\basehighlight{#1}%
%}
}
\newcommand{\highlighttwo}[1]{\textcolor{highlightcolor2}{\textbf{#1}}}

%% bits
\newcommand{\bit}[1]{\textcolor{bitcolor}{\texttt{\upshape #1}}}
\newcommand{\bits}{\{\bit0,\bit1\}}

%%%%%%%%%%%%%%%%%%%%

\usepackage{amsthm}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corol}[theorem]{Corollary}
\newtheorem{assumption}[theorem]{Assumption}
\newtheorem{obs}[theorem]{Observation}
\newtheorem{conj}[theorem]{Conjecture}

\newenvironment{proofof}[1]{\begin{proof}[Proof of #1.]}{\end{proof}}
\newenvironment{proofsketch}{\begin{proof}[Proof Sketch]}{\end{proof}}


%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\namedref}[2]{\hyperref[#2]{#1~\ref*{#2}}}
%% if you don't like it, use this instead:
%\newcommand{\namedref}[2]{#1~\ref{#2}}
\newcommand{\chapterref}[1]{\namedref{Chapter}{#1}}
\newcommand{\sectionref}[1]{\namedref{Section}{#1}}
\newcommand{\theoremref}[1]{\namedref{Theorem}{#1}}
\newcommand{\propositionref}[1]{\namedref{Proposition}{#1}}
\newcommand{\definitionref}[1]{\namedref{Definition}{#1}}
\newcommand{\corollaryref}[1]{\namedref{Corollary}{#1}}
\newcommand{\obsref}[1]{\namedref{Observation}{#1}}
\newcommand{\lemmaref}[1]{\namedref{Lemma}{#1}}
\newcommand{\claimref}[1]{\namedref{Claim}{#1}}
\newcommand{\figureref}[1]{\namedref{Figure}{#1}}
\newcommand{\subfigureref}[2]{\hyperref[#1]{Figure~\ref*{#1}#2}}
\newcommand{\equationref}[1]{\namedref{Equation}{#1}}
\newcommand{\appendixref}[1]{\namedref{Appendix}{#1}}
\newcommand{\tableref}[1]{\namedref{Table}{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%

% prometheus_cv: [2020/10/14 Here to make editing your CV not an eternal torment]
% We customise the \section command
\titleformat{\section}
	{\Large\bfseries\scshape\centering}
	{}{0em}
	{}
	[\titlerule]

% We also slightly update \subsection command
\titleformat{\subsection}
	{\large\scshape\centering}
	{}{0em}
	{}

\setcounter{secnumdepth}{0}

\ifdefined\HCode
\newcommand{\varwidthdiv}[1]{%
	\HCode{<div class="varwidthdiv">}%
	{#1}%
	\HCode{</div>}%
}
\else
\newcommand{\varwidthdiv}[1]{%
	\begin{varwidth}{\linewidth}%
		#1%
	\end{varwidth}%
}
\fi

\reversemarginpar

\ifdefined\HCode
\newcommand{\marginparenum}[1]{%
	\HCode{<div class="marginparenum">}%
	{#1}%
	\HCode{</div>}%
}
\else
\newcommand{\marginparenum}{\marginpar}
\fi

\newcommand{\marginparnewi}{%
	\ifdefined\HCode% Make tex4ht output div not span
	\expandtopar%
	\else
	\leavevmode
	\fi%
}
\newcommand{\marginparnew}[1]{\marginparnewi\marginpar{#1}}
\newcommand{\marginparenumnew}[1]{\marginparnewi\marginparenum{#1}}

\newcommand{\expandtopar}{\par}
\newcommand{\datedsubsectioni}[6]{%
	#5{%
		\marginparnew{%
			\begin{center}%
				\textlf{\footnotesize #1}%
			\end{center}%
		}%
		{#3} \rightalignedtext{\small \textlf{\scshape #2}}%
	}%

	#4%
	\vspace{#6}%
}

\newcommand{\datedsubsection}[4]{\datedsubsectioni{#1}{#2}{#3}{#4}{\cvsubsection}{-1.5ex}}
\newcommand{\datedsubsectionnarrow}[4]{\datedsubsectioni{#1}{#2}{#3}{#4}{\cvsubsectionnarrow}{0.5ex}}

\ifdefined\HCode
\newcommand{\rightalignedtext}[1]{%
	\HCode{<span style="float: right">}%
	{#1}%
	\HCode{</span>}%
}
\else
\newcommand{\rightalignedtext}[1]{%
	\hfill{#1}%
}
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEFINE OUR OWN SUBSECTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ifdefined\HCode
\newcommand\cvsubsection[1]{\vspace{0pt} \leavevmode {#1}}
\newcommand\cvsubsectionnarrow[1]{\vspace{0pt} \leavevmode {#1}}

\else
\newcounter{cvsubsection}
\titleclass{\cvsubsection}{straight}[\part]
\titleformat{\cvsubsection}
	{}
	{}{0em}
	{}
\titlespacing*{\cvsubsection}{0pt}{3.5ex plus 1ex minus .2ex}{0.5ex}

% The same but in narrow
\newcounter{cvsubsectionnarrow}
\titleclass{\cvsubsectionnarrow}{straight}[\part]
\titleformat{\cvsubsectionnarrow}
	{}
	{}{0em}
	{}
\titlespacing*{\cvsubsectionnarrow}{0pt}{0pt}{0pt}

\let\oldcvsubsection\cvsubsection
\let\oldcvsubsectionnarrow\cvsubsectionnarrow
\renewcommand\cvsubsection{\oldcvsubsection*}
\renewcommand\cvsubsectionnarrow{\oldcvsubsectionnarrow*}
\fi


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcounter{publicationCounter}
\newenvironment{enumeratepubs}[1][]{%
	\begin{enumerate}%
		\setcounter{enumi}{\value{publicationCounter}}%
}{%
		\setcounter{publicationCounter}{\value{enumi}}%
	\end{enumerate}%
}

\newcommand{\award}[1]{%
	\marginparenumnew{%
		\begin{flushright}%
		\textcolor{highlightawardtext}{%
			\textlf{\textbf{\footnotesize%
				#1%
			}}%
		}%
		\end{flushright}%
	}%
}

\newcommand{\account}[4]{
	\varwidthdiv{\centering\large%
		{\normalsize #1}~{#2} \\%
		\href{#3}{#4}%
	}%
}
\newcommand{\accountspacer}{\ifdefined\HCode\hspace{0.5em}\else\quad\fi}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\makeatletter
\AtEndPreamble{
\let\runauthor\@author
\let\runtitle\@title
}
\makeatother

\usepackage{fancyhdr}
\fancyhf{}
\rhead{\runtitle}
\lhead{\runauthor}
\rfoot{\thepage}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0pt}

% Provide a default title
\title{}

% From pandoc-crossref I think.
%\makeatletter
%\@ifpackageloaded{subfig}{}{\usepackage{subfig}}
%\@ifpackageloaded{caption}{}{\usepackage{caption}}
%\captionsetup[subfloat]{margin=0.5em}
%\AtBeginDocument{%
%\renewcommand*\figurename{Figure}
%\renewcommand*\tablename{Table}
%}
%\AtBeginDocument{%
%\renewcommand*\listfigurename{List of Figures}
%\renewcommand*\listtablename{List of Tables}
%}
%\@ifpackageloaded{float}{}{\usepackage{float}}
%\floatstyle{ruled}
%\@ifundefined{c@chapter}{\newfloat{codelisting}{h}{lop}}{\newfloat{codelisting}{h}{lop}[chapter]}
%\floatname{codelisting}{Listing}
%\newcommand*\listoflistings{\listof{codelisting}{List of Listings}}
%\makeatother
