BUILD_DIR ?= build
MKDIR_P ?= mkdir -p
LATEXMK ?= latexmk

MAKE4HT_OPTS ?= -u -e html-build.mk4 -c html-build.cfg

PDFLATEX ?= pdflatex %O -halt-on-error -synctex=1 -file-line-error %S

SOURCES      := $(shell find * -path $(BUILD_DIR) -prune -o -regex '.*\.tex' -print)
CSS_SOURCES  := $(shell find * -path $(BUILD_DIR) -prune -o -regex '.*\.css' -print)
FONT_SOURCES := $(shell find * -path $(BUILD_DIR) -prune -o -regex '.*\.\(eot\|svg\|ttf\|woff\|woff2\)' -print)
PAPERS_SRCS  := linear_3d_tensor_field_transition_point.pdf
SOURCES      := $(filter-out headers.tex,$(SOURCES)) $(CSS_SOURCES) $(FONT_SOURCES)

PDFS         := $(BUILD_DIR)/CV.pdf
HTMLS        := $(BUILD_DIR)/index.html
CSSS         := $(addprefix $(BUILD_DIR)/,$(CSS_SOURCES))
FONTS        := $(addprefix $(BUILD_DIR)/,$(FONT_SOURCES))
PAPERS       := $(addprefix $(BUILD_DIR)/,$(PAPERS_SRCS))
TARGETS      := $(PDFS) $(HTMLS)

all : pdf html
.PHONY: all

pdf : $(PDFS)
.PHONY: pdf

html : $(HTMLS) $(CSSS) $(FONTS) $(PAPERS)
.PHONY: html

clean:
	rm -rf $(BUILD_DIR) && rm -f main-html.*
.PHONY: clean

.DELETE_ON_ERROR:

$(BUILD_DIR)/main.pdf $(BUILD_DIR)/main.html : $(SOURCES)

# The touch command is just in case latexmk decides it doesn't need to do anything.
$(BUILD_DIR)/%.pdf : %.tex headers.tex *.bib | $(BUILD_DIR)/
	$(LATEXMK) -pdf -pdflatex='$(PDFLATEX)' $< -jobname=$(basename $@)
	touch $@

$(BUILD_DIR)/%.html : $(BUILD_DIR)/%-html.tex headers.tex html-build.* *.4ht *.bib *.jpg
	make4ht -d $(dir $@) $(MAKE4HT_OPTS) $< mathml,fancylogo "-cunihtf -utf8" && \
	mv $(patsubst %.html,%-html.html,$@) $@

# Currently no modifications for html version.
$(BUILD_DIR)/%-html.tex : %.tex | $(BUILD_DIR)/
	rm -f $@
	cp -al $< $@

# Pass through .css files and fonts. Also papers.
define copy-font-recipe
$(BUILD_DIR)/%.$(1) : %.$(1) | $(BUILD_DIR)/
	rm -f $$@
	cp -al $$< $$@
endef
$(foreach ext,css eot svg ttf woff woff2 pdf,$(eval $(call copy-font-recipe,$(ext))))

$(BUILD_DIR)/index.html : $(BUILD_DIR)/main.html | $(BUILD_DIR)/
	rm -f $@
	cp -al $< $@

$(BUILD_DIR)/CV.pdf : $(BUILD_DIR)/main.pdf | $(BUILD_DIR)/
	rm -f $@
	cp -al $< $@

# Create subdirectories. From https://stackoverflow.com/a/43227047/4071916
$(BUILD_DIR)/:
	mkdir -p $@
$(BUILD_DIR)/%/:
	mkdir -p $@
$(foreach T,$(TARGETS) $(addprefix $(BUILD_DIR)/,$(SOURCES)),$(eval $(T): | $(dir $(T))))
